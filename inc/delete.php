<?php

if (isset($_GET['id'])) {

	require_once 'config.php';

	$conn = new mysqli (SERVER_NAME,USERNAME,PASSWORD,DATABASE);

	if ($conn->connect_error) {
		die('Dogodila se greška:'.$conn->connect_error);
	}

	$id = $_GET['id'];

	$stmt = $conn->prepare('DELETE FROM `list` WHERE `id`=?');
	$stmt->bind_param('i',$id);
	$stmt->execute();

	$conn->close();

	header('Location: ../manage.php');

}else {
	echo 'Brisanje nije uspelo';

}

?>