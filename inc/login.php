<?php

if (!isset($_POST['username']) || !isset($_POST['password'])) {
	die('Dogodila se greška!');
}

$username = htmlentities($_POST['username']);
$password = htmlentities($_POST['password']);

if (empty($username) || empty($password)) {
	echo 'Sva polja moraju biti popunjena!';

}else {

	require_once 'config.php';

	$conn = new mysqli (SERVER_NAME,USERNAME,PASSWORD,DATABASE);

	if ($conn->connect_error){
		die('Dogodila se greška:'.$conn->connect_error);
	}

	//Check user
	$stmt = $conn->prepare('SELECT `password` FROM `admin` WHERE `user` = ?');

	$stmt->bind_param('s',$username);
	$stmt->execute();
	$stmt->bind_result($pass);
	$stmt->fetch();

	//Check password
	//NEED TO HASH
	if ($password === $pass) {
		session_start();
		$_SESSION['manage'] = '?';

		header('Location: ../manage.php');
	}else{
		echo 'Neispravan unos korisničkog imena ili lozinke!';
	}

	$conn->close();
}

?>