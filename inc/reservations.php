<?php

if (!isset($_POST['table']) || !isset($_POST['first_name']) || !isset($_POST['last_name']) || !isset($_POST['email'])){
	die('Dogodila se greška!');

}
	
$table = htmlentities($_POST['table']);
$first_name = htmlentities($_POST['first_name']);
$last_name = htmlentities($_POST['last_name']);
$email = htmlentities($_POST['email']);


if (empty($table) || empty($first_name) || empty($last_name) || empty($email)) {

	echo 'Sva polja moraju biti popunjena!';

}else if ((strpos($table, "Stol")===false && strpos($table, "Sep")===false) || (strlen($table)>6 || strlen($table)<5)) {
	
	echo 'Dogodila se greška!<br><strong>"' .$table. '"</strong> nije validan unos!';

}else if (strlen($first_name)>30 || strlen($last_name)>30 || strlen($email)>40) {
	echo 'Prekoračili ste maksimalan broj znakova!';
	
}else{
/*	
	//Send mail 
	$to = $email;
	$subject = 'Rezervacija na ime: '.$first_name. ' '.$last_name;
	$body = 'Rezervacija: '.$table;
	$body .= "/n";
	$body .= 'Ime: '.$first_name;
	$body .= "/n";
	$body .= 'Prezime: '.$last_name;
	$body .= "/n";
	$body .= "/n";
	$body .= 'Veselimo se Vašem dolasku!';
	$body .= "/n";
	$body .= 'Discotheque PHP';
	$headers = 'From: Discotheque PHP';

	if (mail($to, $subject, $body, $headers)){
*/	

		require_once 'config.php';

		$conn = new mysqli(SERVER_NAME,USERNAME,PASSWORD,DATABASE);

		if ($conn->connect_error) {
			die('Dogodila se greška:'.$conn->connect_error);
		}

		//Check table in db
		$stmt = $conn->prepare("SELECT count(*) FROM `list` WHERE `stol` = ?");
		$stmt->bind_param ("s",$table);
		$stmt->execute();
		$stmt->bind_result($count);
		$stmt->fetch();
		if ($count>0) {
			session_start();
			$message = 'Stol nije slobodan!';
			$_SESSION['table'] = $message;
			header('Location:../index.php');
			die ();  
		}
		$stmt->close();

		//Check email in db
		$stmt2 = $conn->prepare("SELECT count(*) FROM `list` WHERE `email` = ?");
		$stmt2->bind_param ("s",$email);
		$stmt2->execute();
		$stmt2->bind_result($count);
		$stmt2->fetch();
		if ($count>0) {
			session_start();
			$message = 'Nije moguće izvršiti više rezervacija sa iste e-mail adrese!';
			$_SESSION['email'] = $message;
			header('Location:../index.php');
			die ();
		}
		$stmt2->close();

		//Insert reservation
		$stmt3 = $conn->prepare("INSERT INTO `list` (`ime`,`prezime`,`email`,`stol`) VALUES (?,?,?,?)");
		$stmt3->bind_param ("ssss",$first_name,$last_name,$email,$table);
		$stmt3->execute();
		$stmt3->close();

		$conn->close();

		session_start();
		$message = 'Rezervacija izvršena na ime: '.$first_name.' '.$last_name. '.<br>Potvrda rezervacije će biti poslana na Vašu e-mail adresu.';
		$_SESSION['success'] = $message;

     header('Location:../index.php');

     
/*
	}else {
		echo 'Dogodila se greška! Pokušajte ponovo!';
	}
*/	
}



?>