<?php

require_once 'config.php';

$conn = new mysqli(SERVER_NAME,USERNAME,PASSWORD,DATABASE);

		if ($conn->connect_error) {
			die('Dogodila se greška:'.$conn->connect_error);
		}

//Column 'stol' from db as array
$sql = "SELECT `stol` FROM list";
$result = $conn->query($sql);

if (mysqli_num_rows($result) > 0){
	while ($row = mysqli_fetch_assoc($result)){
		$tables[] = $row['stol'];
	}

	
	//Change classes
	echo '<script type=text/javascript>';
	foreach ($tables as $table) {
		echo '$("#'.$table.'").removeClass("table").addClass("reserved");';
	}
	echo '</script>';

 }

 $conn->close();
?>