<?php
session_start();

if (isset($_SESSION['manage'])) {
	
	require_once 'config.php';

	$conn = new mysqli (SERVER_NAME,USERNAME,PASSWORD,DATABASE);

	if ($conn->connect_error) {
		die('Dogodila se greška:'.$conn->connect_error);
	}

	$sql = 'TRUNCATE TABLE `list`';

	if ($conn->query($sql)) {
		header('Location:../manage.php');

	}else {
		echo 'Nije moguće izbrisati sve rezervacije';
	}

	$conn->close();

}


