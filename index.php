<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Rezervacije</title>

	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<h1>Rezervacija stola</h1>
	<p><div class="green"></div> Slobodno</p>
	<p><div class="black"></div> Zauzeto</p>
	
	<svg
	   xmlns:dc="http://purl.org/dc/elements/1.1/"
	   xmlns:cc="http://creativecommons.org/ns#"
	   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	   xmlns:svg="http://www.w3.org/2000/svg"
	   xmlns="http://www.w3.org/2000/svg"
	   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
	   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
	   width="640"
	   height="640"
	   viewBox="0 0 169.33333 169.33334"
	   version="1.1"
	   id="svg8"
	   inkscape:version="0.92.2 (5c3e80d, 2017-08-06)"
	   sodipodi:docname="reservations.svg">
	  <defs
	     id="defs2">
	    <inkscape:perspective
	       sodipodi:type="inkscape:persp3d"
	       inkscape:vp_x="168.53958 : 48.947919 : 1"
	       inkscape:vp_y="0 : 1000 : 0"
	       inkscape:vp_z="-2.1166666 : 103.1875 : 1"
	       inkscape:persp3d-origin="84.666665 : 56.444447 : 1"
	       id="perspective815" />
	  </defs>
	  <sodipodi:namedview
	     id="base"
	     pagecolor="#ffffff"
	     bordercolor="#666666"
	     borderopacity="1.0"
	     inkscape:pageopacity="1"
	     inkscape:pageshadow="2"
	     inkscape:zoom="1"
	     inkscape:cx="351.30435"
	     inkscape:cy="306.00826"
	     inkscape:document-units="mm"
	     inkscape:current-layer="layer1"
	     showgrid="false"
	     units="px"
	     inkscape:pagecheckerboard="false"
	     inkscape:window-width="1920"
	     inkscape:window-height="1017"
	     inkscape:window-x="-8"
	     inkscape:window-y="-8"
	     inkscape:window-maximized="1"
	     showborder="true"
	     inkscape:showpageshadow="false" />
	  <metadata
	     id="metadata5">
	    <rdf:RDF>
	      <cc:Work
	         rdf:about="">
	        <dc:format>image/svg+xml</dc:format>
	        <dc:type
	           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
	        <dc:title></dc:title>
	      </cc:Work>
	    </rdf:RDF>
	  </metadata>
	  <g
	     inkscape:label="Layer 1"
	     inkscape:groupmode="layer"
	     id="layer1"
	     transform="translate(0,-127.66665)">
	    <rect
	       style="opacity:1;fill:#551e1a;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="rect1418"
	       width="221.19167"
	       height="187.325"
	       x="-26.458332"
	       y="121.31665"
	       ry="0.74026185" />
	    <rect
	       style="opacity:1;fill:#787878;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="rect845"
	       width="14.022917"
	       height="103.12103"
	       x="0.019997993"
	       y="152.33942"
	       ry="0.85847676" />
	    <rect
	       style="opacity:1;fill:#787878;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="rect845-1"
	       width="14.022917"
	       height="88.921013"
	       x="155.29042"
	       y="193.5269"
	       ry="0.74026281" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol01"
	       width="14.96"
	       height="14.96"
	       x="43.752705"
	       y="160.93248"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol02"
	       width="14.96"
	       height="14.96"
	       x="77.186684"
	       y="160.93248"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol03"
	       width="14.96"
	       height="14.96"
	       x="110.62063"
	       y="160.93248"
	       ry="0.97485805" 
	       class= "table"/>
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol07"
	       width="14.96"
	       height="14.96"
	       x="43.752705"
	       y="217.28874"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol08"
	       width="14.96"
	       height="14.96"
	       x="77.186684"
	       y="217.28874"
	       ry="0.97485805" 
	       class= "table"/>
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol09"
	       width="14.96"
	       height="14.96"
	       x="110.62063"
	       y="217.28874"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol10"
	       width="14.96"
	       height="14.96"
	       x="43.752705"
	       y="244.8054"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol11"
	       width="14.96"
	       height="14.96"
	       x="77.186684"
	       y="244.8054"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol12"
	       width="14.96"
	       height="14.96"
	       x="110.62063"
	       y="244.8054"
	       ry="0.97485805"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol04"
	       width="14.96"
	       height="14.96"
	       x="43.752705"
	       y="188.44914"
	       ry="0.97485805" 
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol05"
	       width="14.96"
	       height="14.96"
	       x="77.186684"
	       y="188.44914"
	       ry="0.97485805"
	      class= "table" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Stol06"
	       width="14.96"
	       height="14.96"
	       x="110.62063"
	       y="188.44914"
	       ry="0.97485805"
	       class= "table" />
	    <path
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       d="m 132.21328,127.68665 c -0.55401,0 -0.99994,0.44594 -0.99994,0.99994 v 10.70011 c 0,0.554 0.44593,0.99994 0.99994,0.99994 h 23.87089 v 20.1352 c 0,0.71815 0.57841,1.29605 1.29656,1.29605 h 10.63656 c 0.71815,0 1.29604,-0.5779 1.29604,-1.29605 v -21.13514 -4.05402 -6.64609 c 0,-0.554 -0.44593,-0.99994 -0.99993,-0.99994 z"
	       id="Sep02"
	       inkscape:connector-curvature="0"
	       class= "table" />
	    <rect
	       style="opacity:1;fill:#787878;fill-opacity:1;stroke:#000003;stroke-width:0.04228444;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="rect1034"
	       width="2.7780147"
	       height="5.7439561"
	       x="174.62183"
	       y="-331.271"
	       ry="0.28697637"
	       transform="matrix(-0.98447894,0.17550274,-0.59645579,-0.80264593,0,0)" />
	    <rect
	       style="opacity:1;fill:#787878;fill-opacity:1;stroke:#000003;stroke-width:0.04233719;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="rect1034-8"
	       width="2.7773669"
	       height="5.715858"
	       x="-238.52309"
	       y="-319.15964"
	       ry="0.28557247"
	       transform="matrix(-0.98470872,-0.17420891,0.59938799,-0.80045864,0,0)" />
	    <rect
	       style="opacity:1;fill:#4be53d;fill-opacity:1;stroke:#000003;stroke-width:0.04000001;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
	       id="Sep01"
	       width="35.159996"
	       height="12.66"
	       x="58.688694"
	       y="127.68665"
	       ry="0.82498014"
	       class= "table" />
	  </g>
	</svg>

	<form id="reservation" method="POST" action="inc/reservations.php">
		<input type="text" name="table" readonly>
		<input type="text" name="first_name" placeholder="Ime" autofocus required>
		<input type="text" name="last_name" placeholder="Prezime" required>
		<input type="email" name="email" placeholder="Email" required>
		<button type="submit">Rezerviši</button>
	</form>

	<footer>
		<script src='https://code.jquery.com/jquery-3.1.0.min.js' integrity='sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=' crossorigin='anonymous'></script>
		

		<?php  
			require_once 'inc/tables.php';

			if (isset($_SESSION['table'])) {
				echo
				'<div class="message">
					<p>'.$_SESSION['table'].'</p>
				</div>';

			}

			if (isset($_SESSION['email'])) {
				echo
				'<div class="message">
					<p>'.$_SESSION['email'].'</p>
				</div>';

			}

			if (isset($_SESSION['success'])) {
				echo
				'<div class="message">
					<p>'.$_SESSION['success'].'</p>
				</div>';

			}

			session_destroy();
		?>
		<script type="text/javascript" src="js/script.js"></script>

	</footer>

</body>
</html>