<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Upravljnje rezervacijama</title>

	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	
	<?php
	if (isset($_SESSION['manage'])) {

		require_once 'inc/config.php';

		$conn = new mysqli (SERVER_NAME,USERNAME,PASSWORD,DATABASE);

		if ($conn->connect_error){
			die('Dogodila se greška:'.$conn->connect_error);
		}

		//Display reservations

		$sql = 'SELECT `stol`,`ime`,`prezime`,`email`, `id` FROM `list` ORDER BY `stol`';
		$result = $conn->query($sql);

		if (mysqli_num_rows($result)>0) {
			echo 
			'<table>
			<tr>
			<th>Stol</th>
			<th>Ime i prezime</th>
			<th>E-mail</th>
			<th>Brisanje rezervacije</th>
			</tr>';
			while ($row = mysqli_fetch_assoc($result)){
				echo 
				'<tr>
				<td>'.$row['stol'].'</td>
				<td>'.$row['ime'].' '.$row['prezime'].'</td>
				<td>'.$row['email'].'</td>
				<td><button><a href=inc/delete.php?id='.$row['id'].'>Obriši rezervaciju</a></button></td> 
				</tr>';
			}
			echo '</table>';
	?>

			<!--Truncate button-->
			<form method="POST" action="inc/truncate.php">
				<button type="submit">Obriši sve rezervacije</button>
			</form>
	<?php
		}else {
			echo 'Trenutno nemate rezervacija';
		}

		$conn->close();

	}else { 
		session_destroy();
	?>

		<!--Login form-->
		<form method="POST" action="inc/login.php">
			<h2>Upravljaj rezervacijama</h2>
			<input type="text" name="username" placeholder="Korisničko ime"><br>
			<input type="password" name="password" placeholder="Lozinka"><br>
			<button type="submit">Log In</button>
		</form>
	<?php
		}


	?>
</body>
</html>